﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EloiPatreonWatermark : MonoBehaviour {

    public bool m_useWatermark=true; 
    public GameObject m_watermarkPrefab;

    private int m_splashLevel;
	void Awake () {
        DontDestroyOnLoad(this.gameObject);
        m_splashLevel = SceneManager.GetActiveScene().buildIndex;
        if (!m_useWatermark)
            Destroy(this.gameObject);
    }

    private void OnLevelWasLoaded(int level)
    {
        if(level!=m_splashLevel && m_watermarkPrefab!=null)
            Instantiate(m_watermarkPrefab);
    }
    
}
