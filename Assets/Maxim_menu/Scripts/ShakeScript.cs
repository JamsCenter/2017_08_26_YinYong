﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeScript : MonoBehaviour {


	//text shaking
	private float titleXmin, titleXmax, titleYmin, titleYmax;
	public float shakeForce;

	// Use this for initialization
	void Start () {
		shakeForce = 0.5f;
		titleXmin = transform.position.x-shakeForce;
		titleXmax = transform.position.x+shakeForce;
		titleYmin = transform.position.y-shakeForce;
		titleYmax = transform.position.y+shakeForce;
	}
	
	// Update is called once per frame
	void Update () {

		//shake aniamtion
		float shakeH = Random.Range(titleXmin,titleXmax);
		float shakeV = Random.Range(titleYmin,titleYmax);
		transform.position = new Vector3 (shakeH,shakeV,0);
		
	}
}
