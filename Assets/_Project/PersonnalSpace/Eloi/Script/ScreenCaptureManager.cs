﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using System.IO;

public class ScreenCaptureManager : MonoBehaviour {


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
     //   bool ctrlC = Input.GetKeyDown(KeyCode.C);
        IList<Player> players = ReInput.players.GetPlayers();
        bool rewiredCapture = IsRewiredPressingScreenCapture(players);
        if ( rewiredCapture)
        {
            TakeScreenShot();

        }
    }

     public void TakeScreenShot()
    {
        string date = DateTime.Now.ToString("yyyymmdd_HH_MM_ss");
        Directory.CreateDirectory(Application.dataPath + "\\..\\DeathShot\\");
        string path = Application.dataPath + "\\..\\DeathShot\\Win_" + date + ".png";
//#if UNITY_EDITOR
//        path = Application.dataPath + "\\..\\Win_" + date + ".png";
//#endif
        Debug.Log(path);
        ScreenCapture.TakeScreenshotOfMap(path);
    }

    private static bool IsRewiredPressingScreenCapture(IList<Player> players)
    {
        bool rewiredCapture = false;
        for (int i = 0; i < players.Count; i++)
        {
            if (players[i].GetButtonDown("Screenshot"))
            {
                rewiredCapture = true;
                break;
            }
        }

        return rewiredCapture;
    }
}
