﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroy : MonoBehaviour {

    public float _lifeTime=2f;
    public GameObject _toDestroy;

	void  Awake () {
        Destroy(_toDestroy, _lifeTime);
	}
	
}
