﻿// Each #kernel tells which function to compile; you can have many kernels
#pragma kernel CreateBlood
#pragma kernel UpdateBloodTexture
#pragma kernel AStarPropagateForBlack
#pragma kernel AStarPropagateForBlack_PotentialCleanup

// Create a RenderTexture with enableRandomWrite flag and set it
// with cs.SetTexture
RWTexture2D<float4> Result;

Texture2D<float4> bloodTextureFromRenderTexture;
Texture2D<float4> bloodTextureRead;
RWTexture2D<float4> bloodTextureWrite;

StructuredBuffer<float> blackwhiteBufferRead;
RWStructuredBuffer<float> blackwhiteBufferWrite;

Texture2D<float4> propagateBufferRead;
RWTexture2D<float4> propagateBufferWrite;


// 0 or 1
float teamnbr;

int width; 
int height;
float deltaTime;
float blurSpeed;
float bloodDisapearspeed;



[numthreads(8, 8, 1)]
void CreateBlood(uint3 id : SV_DispatchThreadID)
{
	float4 color = bloodTextureFromRenderTexture[id.xy];

	bloodTextureWrite[id.xy] = lerp(bloodTextureRead[id.xy], color, color.a);
	float blackwhite;
	
	if (color.a > 0.01)
	{
		// Where the blood stain is visible, we apply the team 'color'
		blackwhite = teamnbr;
	}
	else
	{
		blackwhite = blackwhiteBufferRead[id.x + id.y * width];
	}

	blackwhiteBufferWrite[id.x + id.y * width] = blackwhite;

	Result[id.xy] = float4(blackwhite, blackwhite, blackwhite,1);
	//Result[id.xy] = float4(id.x,id.y,1,1);
}


/*float Sample(int x, int y)
{
	return blackwhiteBufferRead[index];
}*/


[numthreads(8, 8, 1)]
void UpdateBloodTexture(uint3 id : SV_DispatchThreadID)
{
	float4 col = bloodTextureRead[id.xy];
	col.a = saturate(col.a - bloodDisapearspeed*deltaTime);

	bloodTextureWrite[id.xy] = col;

	//float kernel[25] = { 1,4,6,4,1, 4,16,24,16,4, 6,24,36,24,6, 4,16,24,16,4, 1,4,6,4,1 };
	float kernel[49] = { 0.000036,0.000363,0.001446,0.002291,0.001446,0.000363,0.000036,
		0.000363,0.003676,0.014662,0.023226,0.014662,0.003676,0.000363,
		0.001446,0.014662,0.058488,0.092651,0.058488,0.014662,0.001446,
		0.002291,0.023226,0.092651,0.146768,0.092651,0.023226,0.002291,
		0.001446,0.014662,0.058488,0.092651,0.058488,0.014662,0.001446,
		0.000363,0.003676,0.014662,0.023226,0.014662,0.003676,0.000363,
		0.000036,0.000363,0.001446,0.002291,0.001446,0.000363,0.000036};

	float mean = 0;

	for (int x = 0; x < 7; x++)
	{
		for (int y = 0; y < 7; y++)
		{
			int sampleX = clamp(id.x + x - 3, 1, width-1);
			int sampleY = clamp(id.y + y - 3, 1, 576-1);
			mean += blackwhiteBufferRead[sampleX + sampleY * width] * kernel[x + y * 7];
		}
	}


	mean = (mean - 0.5) * 1.1 + 0.5;

	float self = blackwhiteBufferRead[(id.x - 0) + (id.y + 0) * width];


	self = self*(1 - deltaTime * blurSpeed) + mean * deltaTime * blurSpeed;
	self = saturate(self);

	blackwhiteBufferWrite[(id.x - 0) + (id.y + 0) * width] = self;

	Result[id.xy] = float4(self, self, self, 1);
}

//groupshared shared
//groupshared uint touchedBorder = 0;

//RWBuffer<float> uav;

float4 clusterDetectionStart;


[numthreads(8, 8, 1)]
void AStarPropagateForBlack(uint3 id : SV_DispatchThreadID)
{
	uint3 id2 = id; // *4; Could optimise?
	float left = propagateBufferRead[float2(id2.x - 1, id2.y)].r;
	float right = propagateBufferRead[float2(id2.x + 1, id2.y)].r;
	float up = propagateBufferRead[float2(id2.x, id2.y - 1)].r;
	float down = propagateBufferRead[float2(id2.x, id2.y + 1)].r;

	float rVal = 0;
	if (left > 0 || right > 0 || up > 0 || down > 0) {
		float bwVal = blackwhiteBufferRead[id2.x + id2.y * width];
		if (bwVal < 0.5) {
			rVal = 0.5;
			/*if (left > 0.51 || right > 0.51 || up > 0.51 || down > 0.51) {
				rVal = 1;
			}*/
		}
	}
	//
	// Start point
	if (id2.x == clusterDetectionStart.x && id2.y == clusterDetectionStart.y) {
		rVal = 0.5;
	}
	/*if (id2.x <= 300) {
		float propVal = propagateBufferRead[id.xy].r;
		if (propVal > 0.01) {

			InterlockedAdd(touchedBorder, 1);
			//atomicAdd(touchedBorder, 1); uav[0]
		}
	}

	if (touchedBorder > 4)
	{
		rVal = 1;
	}*/

	propagateBufferWrite[id2.xy] = float4(rVal, 0, 0, 1);
}

[numthreads(8, 8, 1)]
void AStarPropagateForBlack_PotentialCleanup(uint3 id : SV_DispatchThreadID)
{
	float a = propagateBufferRead[float2(1, 1)].r;
	float b = propagateBufferRead[float2(1, 576 / 4 * 1)].r;
	float c = propagateBufferRead[float2(1, 576 / 4 * 2)].r;
	float d = propagateBufferRead[float2(1, 576 / 4 * 3)].r;
	float e = propagateBufferRead[float2(1, 576)].r;

	if (a > 0 || b > 0 || c > 0 || d > 0 || e > 0) {
		// We propagated till the border, things are good
		blackwhiteBufferWrite[id.x + id.y * width] = blackwhiteBufferRead[id.x + id.y * width];
	}
	else
	{
		// Need cleanup!
		if (propagateBufferRead[id.xy].r > 0.01) {
			blackwhiteBufferWrite[id.x + id.y * width] = 1;
		}
		else {
			blackwhiteBufferWrite[id.x + id.y * width] = blackwhiteBufferRead[id.x + id.y * width];
		}
	}
	propagateBufferWrite[id.xy] = float4(0, 0, 0, 0); // This is a one time function.
}
